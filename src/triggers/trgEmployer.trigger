trigger trgEmployer on Employer__c (Before Update,Before Delete) {
//Before Update
if(Trigger.isUpdate&&Trigger.isBefore)
{
for(Employer__c objEmp:Trigger.New)
{
if(objEmp.Date_of_Establishment__c==null)
{
objEmp.Date_of_Establishment__c=Date.valueof(objEmp.createdDate+10);//Date.Today();
}
if(objEmp.Website__c==null)
{
objEmp.Website__c='www.facebook.com';
}
/*
if(Annual_revenue__c==null)
{
objEmp.Annual_revenue__c='10000';
}*/
}

}//Before Delete
if(Trigger.isDelete&&Trigger.isBefore)
{
for(Employer__c objEmp:Trigger.New)
{
objEmp.addError('You cannot delete the record');
}
}
}